package repository

import (
	"context"
	"fmt"
	"log"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// Config for DB
type Config struct {
	Domain string
	User   string
	Pw     string
	DB     string
	Port   string
}

// DB struct for data store
type DB struct {
	conf        Config
	client      *mongo.Client
	db          *mongo.Database
	Collections map[string]*mongo.Collection
}

// New return new repository
func New(ctx context.Context, config Config, db string, collections []string) (repository *DB, err error) {

	repository = new(DB)
	repository.conf = config

	if repository.conf.User == "dev" {
		repository.client, err = mongo.NewClient(options.Client().ApplyURI("mongodb://127.0.0.1:27017"))
	} else {
		repository.client, err = mongo.NewClient(options.Client().ApplyURI(fmt.Sprintf("%s://%s:%s@%s:%s", repository.conf.Domain, repository.conf.User, repository.conf.Pw, repository.conf.DB, repository.conf.Port)))
	}

	if err != nil {
		log.Fatal(err)
	}

	err = repository.client.Connect(ctx)
	if err != nil {
		log.Fatal(err)
	}

	// defer repository.client.Disconnect(nil)
	repository.db = repository.client.Database(db)
	repository.Collections = make(map[string]*mongo.Collection)
	for _, key := range collections {
		repository.Collections[key] = repository.db.Collection(key)
	}

	return repository, nil
}
